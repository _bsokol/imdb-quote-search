module.exports = function () {
  return {
    options: {
      spawn: false
    },
    eslint: {
      files: ['<%= pathTo.srcJs %>'],
      tasks: ['eslint']
    }
  };
};
