const querystring = require('querystring');
const xray        = new (require('x-ray'))();

module.exports.quoteSearch = function(event, context) {

  const quote = typeof event.query !== 'undefined' ? event.q : '';

  if (!quote.length) {
    context.fail('No quote provided');
    return;
  }

  const url = 'http://www.imdb.com/search/text?'
    + querystring.stringify({
      realm: 'title',
      field: 'quotes',
      q: quote
    });

  console.log('Searching:', quote);
  console.log('URL:', url);

  xray(url, '#main .results tr', [
    {
      title: '.title a',
      episode: '.episode a',
      year: '.year_type'
    }
  ])(function(err, res) {
    if (err) {
      context.fail(err);
    } else {
      const limitedResult = res.slice(0, 5);
      console.log('Result: ', res.length, limitedResult);
      context.succeed(limitedResult);
    }
  });

};
