module.exports = function (grunt) {
  var paths = require('./paths');
  require('load-grunt-tasks')(grunt);

  var tasks = require('load-grunt-configs')(grunt, {
    config: {
      src: ['tasks/*.js']
    },
    pathTo: paths
  });

  grunt.initConfig(tasks);

  grunt.registerTask('default', ['eslint', 'clean', 'webpack', 'watch']);
};
