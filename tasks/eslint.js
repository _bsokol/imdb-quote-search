module.exports = function() {
  return {
    options: {
      configFile: '.eslintrc'
      //force: true //do not fail on errors
    },
    target: ['<%= pathTo.srcJs %>']
  };
};
