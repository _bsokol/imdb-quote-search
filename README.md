# IMDB Quote Search

**IMDB Quote Search** is designed to be used as an AWS Lambda function. It takes a text string and queries IMDB for movies containing that string as a quote.

This project is a component of the Slalom 2016 Amazon Echo Hackathon project *Name that Movie*.

## Prerequisites

This project is intended to be run on Node.js 0.10.36. Grunt and npm are required for building the project.

## Building

First, install dependencies:

    npm install

Then run grunt to build the file that can be uploaded to Lambda:

    grunt
    
This will result in the file `dist/quote-search.js`.

Eslint will run a static check before building. This app uses Webpack to ensure that everything will be self-contained in a single file. To aid with development, Grunt will remain in watch-mode and retranspile the code if you make changes.

## Testing

haha

## Deploying
The name of the Lambda handler function is `quoteSearch`.

It is expecting the search term to be sent in an event param called `query`.

## Return Value

If everything goes well, it will return an array of objects containing up to 5 results. They will be ordered however IMDB wants to order them, which is probably by popularity. The array may be empty if the quote is not found. Each object in the array will contain the following keys:

- **title** - The title of the movie, TV show, video game, etc.
- **episode** - If it's an episodic medium, this value will have the episode name. This key will not exist otherwise.
- **year** - The year the show, movie, etc. premiered. Sometimes this value also contains a category.

For example, a query for **"I am your father"** returns:

    [
      {
        "title": "The Flash",
        "episode": "Tricksters",
        "year": "(2014 TV Series)"
      },
      {
        "title": "Star Wars: Episode V - The Empire Strikes Back",
        "year": "(1980)"
      }
    ]
    
If there is a problem, Lambda will report it as an error that contains some text hopefully explaining what went wrong. Possible errors include an inability to retrieve or parse results from IMDB or an empty value being passed in as the query.

It also logs requests and errors to the console.