var webpack = require('webpack');

module.exports = function() {
  return {
    options: {
      cache: true,
      entry: '<%= pathTo.entry %>',
      output: {
        path: '<%= pathTo.dist %>',
        filename: 'quote-search.js',
        library: "quote-search",
        libraryTarget: "commonjs2"
      },
      target: 'node',
      plugins: [
        new webpack.DefinePlugin({
          'global.GENTLY': false
        }),
        new webpack.IgnorePlugin(/^emitter/)
      ],
      module: {
        loaders: [
          {
            loader: 'babel-loader',
            test: /\.js$/,
            include: [
              '<%= pathTo.srcJs %>'
            ],
            exclude: /(node_modules)/,
            query: {
              presets: ['es2015']
            }
          },
          {
            loader: "json-loader",
            test: /\.json/
          }
        ]
      }
    },
    prod: {
      plugins: [
        new webpack.DefinePlugin({
          'process.env': {
            'NODE_ENV': JSON.stringify('production')
          }
        }),
        new webpack.optimize.OccurenceOrderPlugin(),
        new webpack.optimize.DedupePlugin()
      ]
    }
  };
};
