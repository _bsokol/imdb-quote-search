var path = require('path');

var pathTo       = {};
pathTo['base']   = __dirname;
pathTo['src']    = path.join(pathTo.base, 'app');
pathTo['dist']   = path.join(pathTo.base, 'dist');
pathTo['srcJs']  = path.join(pathTo.src, '**', '*.js');
pathTo['distJs'] = path.join(pathTo.base, 'dist');
pathTo['entry']  = path.join(pathTo.src, 'index.js');

module.exports = pathTo;
